import React from "react";
import styled from "styled-components";
import {SearchInputColor, SearchInputPlaceholderColor, SearchInputTextColor} from "./Colors";

interface Suggestion {
    id: number
    value: string
}

interface AutocompleteProps {
    suggestions: Array<Suggestion> | undefined
    elementSelected: (index: number, value: string) => void
}

interface AutocompleteState {
    activeSuggestion: number
    filteredSuggestions: Array<Suggestion>
    showSuggestions: boolean
    userInput: string
}

const AutocompleteElement = styled.div`
    position: relative;
    display: inline-block;
`;

const SearchInput = styled.input`
    padding: 0px 15px 0px 50px;
    font-size:24px;
    color: ${SearchInputTextColor};
/*removing boder from search box*/
    border:none;
    background-repeat:no-repeat;
/*background-size*/
-webkit-background-size:35px 35px;
   -moz-background-size:35px 35px;
     -o-background-size:35px 35px;
        background-size:35px 35px;
/*positioning background image*/
    background-position:8px 12px;
/*changing background color form white*/
    background-color: ${SearchInputColor};
    ::-webkit-input-placeholder{
        color: ${SearchInputPlaceholderColor};
    }
    :-moz-placeholder { /* Firefox 18- */
       color: ${SearchInputPlaceholderColor};
    }
    ::-moz-placeholder {  /* Firefox 19+ */
       color: ${SearchInputPlaceholderColor};
    }
    :-ms-input-placeholder {  /* interner explorer*/
       color: ${SearchInputPlaceholderColor};
    }
`;

const SuggestionBox = styled.ul`
    list-style: none;
    padding: 0;
    width: 100%;
    position: absolute;
    z-index: 1;
    margin: 0;
    border: 1px solid black;
    background-color: white;
`;

const SuggestionElement = styled.li`
    margin-bottom: 5px;
    :hover{
        transform:translateX(20px);
    }
    overflow:hidden;
    transition:all 0.3s ease-in-out;
`;

const NoSuggestion = styled.div`
    padding:0;
    width:465px;
`;

const NoSuggestionElement = styled.em`
    margin-bottom:10px;
    height: 63px;
`;

export default class Autocomplete extends React.Component<AutocompleteProps, AutocompleteState>{
    constructor(props: AutocompleteProps) {
        super(props);
        this.state = {
            activeSuggestion: 0,
            filteredSuggestions: [],
            showSuggestions: false,
            userInput: ''
        }
    }

    onChange(userInput: string) {
        if(userInput === "") {
            this.setState({
                activeSuggestion: 0,
                showSuggestions: false,
                filteredSuggestions: [],
                userInput: userInput
            });
            return
        }

        const filteredSuggestions = this.props.suggestions?.filter(
            (suggestion) =>
                suggestion.value.toLowerCase().indexOf(userInput.toLowerCase()) > -1
        );

        if (!filteredSuggestions) {
            this.setState({
                activeSuggestion: 0,
                showSuggestions: false,
                filteredSuggestions: [],
                userInput: userInput
            });
            return
        }

        this.setState({
            activeSuggestion: 0,
            showSuggestions: true,
            filteredSuggestions: filteredSuggestions,
            userInput: userInput
        });
    }

    onClick(value: string) {
        this.setState({
            activeSuggestion: 0,
            showSuggestions: false,
            filteredSuggestions: [],
            userInput: value
        });
        let suggestions = this.props.suggestions?.filter((suggestion) => suggestion.value.toLowerCase().indexOf(value.toLowerCase()) > -1);
        if (suggestions && suggestions.length === 1) {
            const {id, value} = suggestions[0];
            this.props.elementSelected(id, value)
        }
    }

    render() {
        let suggestionsListComponent;
        if (this.state.showSuggestions) {
            if (this.state.filteredSuggestions.length) {
                suggestionsListComponent = (
                    <SuggestionBox>
                        {this.state.filteredSuggestions.map((suggestion, index) => {

                            return (
                                <SuggestionElement  key={suggestion.id} onClick={(ev) => {
                                    this.onClick(ev.currentTarget.innerText);
                                }}>
                                    {suggestion.value}
                                </SuggestionElement>
                            );
                        })}
                    </SuggestionBox>
                );
            } else {
                suggestionsListComponent = (
                    <NoSuggestion className="no-suggestions">
                        <NoSuggestionElement>No suggestions!</NoSuggestionElement>
                    </NoSuggestion>
                );
            }
        }

        return (
            <AutocompleteElement>
                <SearchInput
                    type="text"
                    onChange={(ev) => {
                        this.onChange(ev.currentTarget.value)
                    }}

                    onFocus={() => {
                        if (this.state.filteredSuggestions.length) {
                            this.setState({showSuggestions: true})
                        }
                    }}

                    value={this.state.userInput}
                    placeholder="search..."
                />
                {suggestionsListComponent}
            </AutocompleteElement>
        );
    }
}