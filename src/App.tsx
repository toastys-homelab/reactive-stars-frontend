import React from 'react';
import styled from 'styled-components';
import {BrowserRouter as Router, Link, Route} from "react-router-dom";
import Shipyard from "./Shipyard";
import {
    OpenFlowLabsDarkColor,
    OpenFlowLabsDarkItemBorderColor,
    OpenFlowLabsDarkTextColor,
    OpenFlowLabsMainColor
} from "./Colors";
import ApolloClient from "apollo-client";

const AppBody = styled.div`
  display: grid;
  grid-template-areas: "header header header"
                       "sidebar content content"
                       "sidebar content content";
  grid-template-rows: 50px 1fr 1fr;
  grid-template-columns: 300px 1fr 1fr;
  height: 100vh;
`;

const AppHeader = styled.div`
    grid-area: header;
    background-color: ${OpenFlowLabsMainColor};
`;

const AppSidebar = styled.div`
    grid-area: sidebar;
    background-color: ${OpenFlowLabsDarkColor};
`;

const AppContent = styled.div`
    grid-area: content;
`;

const SideBarMenuWrapper = styled.div`
    width: 100%;
`;

const SideBarMenuItem = styled.div`
    border-bottom: 2px solid ${OpenFlowLabsDarkItemBorderColor};
    width: 100%;
    justify-content: center;
    display: flex;
    align-items: center;
    height: 4em;
`;

const SideBarMenuItemLink = styled(Link)`
    color: ${OpenFlowLabsDarkTextColor};
    text-decoration: none;
    font-size: 32px;
    margin: 0;
`;

interface MenuProps {
    items: [
        {
            name: string
            path: string
        }
    ]
}

class SideBarMenu extends React.Component<MenuProps> {
    render(): React.ReactElement {
        return (
            <SideBarMenuWrapper>
                {this.props.items.map(function (item, index) {
                    return <SideBarMenuItem key={index}>
                        <SideBarMenuItemLink to={item.path}>{item.name}</SideBarMenuItemLink>
                    </SideBarMenuItem>
                })}
            </SideBarMenuWrapper>
        )
    }
}

interface AppProps {
    client: ApolloClient<any>
}

function App(props: AppProps) {
    return (
      <AppBody>
          <Router>
          <AppHeader>
              <p>Header</p>
          </AppHeader>
          <AppSidebar>
              <SideBarMenu items={
                  [
                      {
                          name: "Shipyard",
                          path: "/shipyard"
                      }
                  ]
              }/>
          </AppSidebar>
          <AppContent>
            <Route exact path="/shipyard">
                <Shipyard client={props.client} />
            </Route>
          </AppContent>
          </Router>
      </AppBody>
  );
}

export default App;
