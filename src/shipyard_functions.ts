export enum ShipClass {
    Fighter ,
    Corvette,
    Frigate,
    Cruiser,
    Capital
}

export enum TechLevel {
    TL1 = 1,
    TL2 = 2,
    TL3 = 3,
    TL4 = 4,
    TL5 = 5
}

export enum ShipType {
    Military = "Military",
    Civilian = "Civilian",
}

export class Hull {
    name: string = "";
    cost: number = 0;
    speed: number = 0;
    armor: number = 0;
    hit_points: number = 0;
    minimal_crew: number = 0;
    maximal_crew: number = 0;
    armor_class: number = 0;
    power: number = 0;
    mass: number = 0;
    hard_points: number = 0;
    class: ShipClass = 0;
    price_modifier: number = 0;
    mass_modifier: number = 0;
    cargo_modifier: number = 0;
}

export class Drive {
    name: string = "";
    cost: number = 0;
    power: number = 0;
    mass: number = 0;
    required_class: ShipClass = 0;
    tech_level: TechLevel = 0;
}

export class Armament {
    name: string = "";
    cost: number = 0;
    ammo_cost: number = 0;
    damage: string = "";
    power: number = 0;
    mass: number = 0;
    hard_points: number = 0;
    required_class: ShipClass = 0;
    tech_level: TechLevel = 0;
    armor_penetration: number = 0;
    special: string = "";
    ammo: number = 0;
}

export class Defense {
    name: string = "";
    cost: number = 0;
    power: number = 0;
    mass: number = 0;
    required_class: ShipClass = 0;
    tech_level: TechLevel = 0;
    armor_class_modifier: number = 0;
    hit_point_modifier: number = 0;
    speed_modifier: number = 0;
    ammo: number = 0;
    special_effect: string = "";
}

export class Fitting {
    name: string = "";
    cost: number = 0;
    power: number = 0;
    mass: number = 0;
    required_class: ShipClass = 0;
    tech_level: TechLevel = 0;
    notes: string = "";
    cargo_modifier: number = 0;
}

export interface ShipyardData {
    hulls: Hull[]
    drives: Drive[]
    armaments: Armament[]
    defenses: Defense[]
    fittings: Fitting[]
}

export class Ship {
    class_name: string = "";
    build_date: string = "";
    shipyard: string = "";
    designer: string = "";
    crew: number = 0;
    credit_stores: number = 0;
    ship_type: ShipType = ShipType.Military;
    hull: Hull = new Hull();
    drives: {
        drive: Drive
        amount: number
    }[] = [];
    armaments: {
        armament: Armament
        amount: number
    }[] = [];
    defenses: Defense[] = [];
    fittings: {
        fitting: Fitting
        amount: number
    }[] = [];
    setHull = (hull: Hull | undefined) => {
        if (!hull){
            return
        }
        this.hull = hull;
        this.crew = this.hull.minimal_crew;
    };
    getCost = () => {
        let cost = this.hull.cost;
        for (let i=0;i < this.drives.length;i++){
            const item = this.drives[i];
            cost += (item.drive.cost * this.hull.price_modifier)
        }

        for (let i=0;i < this.defenses.length;i++){
            const item = this.defenses[i];
            cost += (item.cost * this.hull.price_modifier)
        }

        for (let i=0;i < this.fittings.length;i++){
            const item = this.fittings[i];
            cost += (item.fitting.cost * item.amount)
        }

        for (let i=0;i < this.armaments.length;i++){
            const item = this.armaments[i];
            cost += ((item.armament.cost * item.amount) + (item.armament.ammo_cost*item.armament.ammo*item.amount))
        }

        return cost;
    };
    getFreePower = () => {
        let power = this.hull.power;
        for (let i=0;i < this.drives.length;i++){
            const item = this.drives[i];
            power -= item.drive.power
        }

        for (let i=0;i < this.defenses.length;i++){
            const item = this.defenses[i];
            power -= item.power
        }

        for (let i=0;i < this.fittings.length;i++){
            const item = this.fittings[i];
            power -= (item.fitting.power * item.amount)
        }

        for (let i=0;i < this.armaments.length;i++){
            const item = this.armaments[i];
            power -= (item.armament.power * item.amount)
        }

        return power
    };
    getFreeMass = () => {
        let mass = this.hull.mass;
        for (let i=0;i < this.drives.length;i++){
            const item = this.drives[i];
            mass -= (item.drive.mass * this.hull.mass_modifier)
        }

        for (let i=0;i < this.defenses.length;i++){
            const item = this.defenses[i];
            mass -= (item.mass * this.hull.mass_modifier)
        }

        for (let i=0;i < this.fittings.length;i++){
            const item = this.fittings[i];
            mass -= (item.fitting.mass * item.amount)
        }

        for (let i=0;i < this.armaments.length;i++){
            const item = this.armaments[i];
            mass -= ((item.armament.mass * item.amount)+(item.armament.ammo*item.amount))
        }

        return mass;
    };
    getFreeHardPoints = ()  => {
        let hard_points = this.hull.hard_points;
        for (let i=0;i < this.armaments.length;i++){
            const item = this.armaments[i];
            hard_points -= (item.armament.hard_points * item.amount)
        }
        return hard_points;
    };
    getSpeed = () => {
        let speed = this.hull.speed;
        for (let i=0;i < this.defenses.length;i++){
            const item = this.defenses[i];
            speed -= item.speed_modifier
        }
        return speed;
    };
    getHitPoints = () => {
        let hp = this.hull.hit_points;
        for (let i=0;i < this.defenses.length;i++){
            const item = this.defenses[i];
            hp += item.hit_point_modifier
        }
       return hp;
    };
    getMaintenanceCost = () => {

    };
    getArmorClass = () => {
        let ac = this.hull.armor_class;
        for (let i=0;i < this.defenses.length;i++){
            const item = this.defenses[i];
            ac -= item.armor_class_modifier;
        }
        return ac;
    };
    getCargoSpace = () => {
        let cargo_space = 0;
        for (let i=0;i < this.fittings.length;i++){
            const item = this.fittings[i];
            cargo_space += ((item.fitting.cargo_modifier * this.hull.cargo_modifier) * item.amount)
        }
        return cargo_space;
    };
}
