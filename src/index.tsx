import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
//import { HttpLink } from 'apollo-link-http';
import { SchemaLink } from 'apollo-link-schema';
import { ApolloProvider } from '@apollo/react-hooks';
import { makeExecutableSchema } from '@graphql-tools/schema';
import { addMocksToSchema } from '@graphql-tools/mock';
import { loader } from 'graphql.macro';
import mocks from "./mocks";

const schemaString = loader('../schema.graphql')
const schema = makeExecutableSchema({ typeDefs: schemaString });
const schemaWithMocks = addMocksToSchema({ schema, mocks });

const cache = new InMemoryCache();
// const link = new HttpLink({
//     uri: 'http://localhost:3000/query'
// });

const link = new SchemaLink({
    schema: schemaWithMocks
})

const client = new ApolloClient({
    cache,
    link
});


ReactDOM.render(
    <ApolloProvider client={client}>
        <React.StrictMode>
            <App client={client}/>
        </React.StrictMode>
    </ApolloProvider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
