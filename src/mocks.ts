import {Hull, Ship, ShipType} from "./shipyard_functions";
import shipyardData from './shipyard.json';
import faker from 'faker'
import {v4 as uuidv4} from 'uuid';

const freeMerchant = shipyardData.hulls.filter((hull) => hull.name === "Free Merchant")[0];

const hull: Hull = Object.assign(new Hull(), freeMerchant);

const sampleShip: Ship = Object.assign(new Ship(), {
    hull: hull,
    crew: hull.minimal_crew,
    class_name: "Beowulf",
    ship_type: ShipType.Civilian,
    build_date: faker.date.recent(),
    shipyard: "Mockyard Earth LLC",
    designer: "Atlan Design inc.",
    drives: [{
        drive: shipyardData.drives.filter((drive) => drive.name === "Spike-1")[0],
        amount: 1,
    }],
    armaments: [
        {
            armament: shipyardData.armaments.filter((arm) => arm.name === "Multifocal Laser")[0],
            amount: 2,
        },
    ],
    fittings: [
        {
            fitting: shipyardData.fittings.filter((fit) => fit.name === "Cargo Space")[0],
            amount: 11,
        },
    ],
    defenses: shipyardData.defenses.filter((def) => def.name === "Augmented Plating")
});

const mocks = {
    UUID: () => {
        return uuidv4();
    },
    Time: () => {
        return faker.time.recent();
    },
    Ship: () => {
        return sampleShip;
    },
};

export default mocks;