
//Colors from the Shipyard excel file
export const ShipyardLightBrown = "#eeece1";
export const ShipyardDarkBrown = "#938953";
export const ShipyardLightGreen = "#d6e3bc";
export const ShipyardLightBlue = "#b6dde8";
export const ShipyardGrey = "#f2f2f2";

// Colors for the Autocomplete fields
export const SearchInputColor = "#7accc8";
export const SearchInputTextColor= "#1f5350";
export const SearchInputPlaceholderColor = "#b1e0de";

// Main app colors used by all apps
export const OpenFlowLabsMainColor = "#743287";
export const OpenFlowLabsDarkColor = "#22243e";
export const OpenFlowLabsDarkItemBorderColor = "#233e4d";
export const OpenFlowLabsDarkTextColor = "#ffffff";