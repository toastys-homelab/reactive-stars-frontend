import React from "react";
import styled from "styled-components";
import {Ship, ShipyardData} from "./shipyard_functions";
import Autocomplete from "./Autocomplete";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import { faCoffee } from '@fortawesome/free-solid-svg-icons'
import gql from "graphql-tag";
import ApolloClient from "apollo-client";

const ShipSheetWrapper = styled.div`
    padding: 10px;
    border: 2px solid;
    margin: 20px;
    height: 92%;
    font-size: 20px;
    display: grid;
    grid-template-areas: "title stats stats"
                         "weapons weapons weapons"
                         "defenses defenses defenses"
                         "fittings fittings fittings"
                         "shipyard shipyard shipyard"
                         "description description description";
    grid-template-rows: repeat(6, 1fr);
    grid-template-columns: repeat(3, 1fr);
`;

const ShipNameWrapper = styled.div`
    grid-area: title;
`;

const ShipClassName = styled.span`
    font-style: italic;
    font-weight: bold;
`;
const ShipName = styled.span`
    margin-left: 10px;
`;

const ShipStatsContainer = styled.div`
    display: grid;
    grid-template-columns: repeat(3, 1fr);
    grid-area: stats;
`;

const ShipStatWrapper = styled.div`
`;

const ShipStatName = styled.span`
    font-style: italic;
    font-weight: bold;
    margin-right: 10px;
`;

const ShipStateDataItem = styled.span``;

const ListTitle = styled.div`
    font-style: italic;
    font-weight: bold;
    grid-area: title;
`;

const ListContainer = styled.div`
    grid-area: list;
`;

interface ShipyardProps {
    client: ApolloClient<any>
}

interface ShipyardState {
    ship: Ship
    showHullSelector: boolean
    data: ShipyardData | null
}

const GET_SHIPYARD_DATA = gql`
    {
        data{
            hulls{
                id
                name
                cost
                power
                mass
                class
                speed
                armor
                hit_points
                minimal_crew
                maximal_crew
                armor_class
                hard_points
                price_modifier
                mass_modifier
                cargo_modifier
            }
            drives {
                id
                name
                cost
                power
                mass
                required_class
                tech_level
            }
            armaments {
                id
                name
                cost
                ammo_cost
                ammo
                damage
                power
                mass
                hard_points
                required_class
                tech_level
                armor_penetration
                special
            }
            defenses {
                id
                name
                cost
                power
                mass
                required_class
                tech_level
                armor_class_modifier
                hit_point_modifier
                speed_modifier
                ammo
                special_effect
            }
            fittings {
                id
                name
                cost
                mass
                power
                required_class
                tech_level
                notes
                cargo_modifier
            }
        }
    }
`;

const GET_SHIP = gql`
    query FullShipData($shipId: UUID!){
        getShip(id: $shipId) {
            id
            class_name
            build_date
            shipyard
            designer
            crew
            credit_stores
            ship_type
            hull{
                id
                name
                cost
                power
                mass
                class
                speed
                armor
                hit_points
                minimal_crew
                maximal_crew
                armor_class
                hard_points
                price_modifier
                mass_modifier
                cargo_modifier
            }
            drives {
                amount
                drive{
                    id
                    name
                    cost
                    power
                    mass
                    required_class
                    tech_level
                }
            }
            armaments {
                amount
                armament {
                    id
                    name
                    cost
                    ammo_cost
                    ammo
                    damage
                    power
                    mass
                    hard_points
                    required_class
                    tech_level
                    armor_penetration
                    special
                }
            }
            defenses {
                id
                name
                cost
                power
                mass
                required_class
                tech_level
                armor_class_modifier
                hit_point_modifier
                speed_modifier
                ammo
                special_effect
            }
            fittings {
                amount
                fitting {
                    id
                    name
                    cost
                    mass
                    power
                    required_class
                    tech_level
                    notes
                    cargo_modifier
                }
            }
        }
    }
`;

export default class Shipyard extends React.Component<ShipyardProps, ShipyardState> {
    constructor(props: any) {
        super(props);
        this.state = {
            data: null,
            showHullSelector: false,
            ship: new Ship(),
        };
    }

    // componentDidMount(): void {
    //     axios.get("/db").then((resp) => {
    //         let data: ShipyardData = resp.data;
    //         this.setState({
    //             data: data,
    //         });
    //
    //         let ship = this.state.ship;
    //         for (let i=0; i < data.armaments.length; i++) {
    //             if (data.armaments[i].name === "Multifocal Laser" || data.armaments[i].name === "Sand Thrower") {
    //                 ship.armaments.push({
    //                     amount: 1,
    //                     armament: data.armaments[i],
    //                 })
    //             }
    //         }
    //
    //         for (let i=0; i < data.fittings.length; i++) {
    //             if (data.fittings[i].name === "Atmospheric Configuration" || data.fittings[i].name === "Fuel Scoops"|| data.fittings[i].name === "Fuel Bunkers") {
    //                 ship.fittings.push({
    //                     amount: 1,
    //                     fitting: data.fittings[i],
    //                 })
    //             }
    //
    //             if (data.fittings[i].name === "Cargo Space") {
    //                 ship.fittings.push({
    //                     amount: 8,
    //                     fitting: data.fittings[i],
    //                 })
    //             }
    //         }
    //
    //         for (let i=0; i < data.hulls.length; i++) {
    //             if (data.hulls[i].name === "Free Merchant") {
    //                 ship.setHull(data.hulls[i]);
    //             }
    //         }
    //
    //         this.setState({
    //             ship: ship,
    //         });
    //     }).catch(function (err) {
    //         console.log(err);
    //     });
    // }

    componentDidMount(): void {
        this.props.client.query({query: GET_SHIPYARD_DATA}).then((resp) => {
            if (resp.errors) {
                console.log(resp.errors);
            }

            this.setState({
                data: resp.data.data
            });
        }).catch((err) => {
            console.log(err)
        });

        this.props.client.query({
            query: GET_SHIP,
            variables: {
                shipId: "041d4c48-bfb5-4a51-84b7-149d4be9e173",
            }
        }).then((resp) => {
            this.setState({
                ship: Object.assign(new Ship(), resp.data.getShip)
            })
        }).catch((err) => {
            console.log(err)
        })
    }

    hullSelected(index: number,value: string) {
        let ship = this.state.ship;
        let hull = this.state.data?.hulls[index];
        if(!hull) {
            return
        }

        ship.setHull(hull);
        this.setState({
            ship: ship,
            showHullSelector:false,
        })
    }

    render(): React.ReactElement {
        let shipNameElement;
        if (this.state.ship.hull && !this.state.showHullSelector) {
            shipNameElement = <ShipName
                onClick={()=>{this.setState({showHullSelector: true})}}
            >
                {this.state.ship.hull.name}
            </ShipName>
        } else {
            shipNameElement = <Autocomplete
                suggestions={this.state.data?.hulls.map((hull, idx) => {
                    return {id: idx, value: hull.name}
                })}

                elementSelected={this.hullSelected.bind(this)}
            />
        }

        return (
            <ShipSheetWrapper>
                <ShipNameWrapper>
                    <ShipClassName>{this.state.ship.class_name}-class</ShipClassName>
                    {shipNameElement}
                    <FontAwesomeIcon icon={faCoffee} />
                </ShipNameWrapper>
                <ShipStatsContainer>
                    <ShipStatWrapper>
                        <ShipStatName>Cost:</ShipStatName>
                        <ShipStateDataItem>{this.state.ship.getCost().toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1´")} Cr</ShipStateDataItem>
                    </ShipStatWrapper>
                    <ShipStatWrapper>
                        <ShipStatName>Speed:</ShipStatName>
                        <ShipStateDataItem>{this.state.ship.getSpeed()}</ShipStateDataItem>
                    </ShipStatWrapper>
                    <ShipStatWrapper>
                        <ShipStatName>Hit Points:</ShipStatName>
                        <ShipStateDataItem>{this.state.ship.getHitPoints()}</ShipStateDataItem>
                    </ShipStatWrapper>
                    <ShipStatWrapper>
                        <ShipStatName>Armor:</ShipStatName>
                        <ShipStateDataItem>{this.state.ship.hull.armor}</ShipStateDataItem>
                    </ShipStatWrapper>
                    <ShipStatWrapper>
                        <ShipStatName>Power:</ShipStatName>
                        <ShipStateDataItem>{this.state.ship.hull.power}/ {this.state.ship.getFreePower()}</ShipStateDataItem>
                        <span  style={{marginLeft: "10px"}}>free</span>
                    </ShipStatWrapper>
                    <ShipStatWrapper>
                        <ShipStatName>Hard Points:</ShipStatName>
                        <ShipStateDataItem>{this.state.ship.hull.hard_points}/ {this.state.ship.getFreeHardPoints()}</ShipStateDataItem>
                        <span  style={{marginLeft: "10px"}}>free</span>
                    </ShipStatWrapper>
                    <ShipStatWrapper>
                        <ShipStatName>Crew:</ShipStatName>
                        <ShipStateDataItem>{this.state.ship.crew}/ {this.state.ship.hull.maximal_crew}</ShipStateDataItem>
                    </ShipStatWrapper>
                    <ShipStatWrapper>
                        <ShipStatName>AC:</ShipStatName>
                        <ShipStateDataItem>{this.state.ship.getArmorClass()}</ShipStateDataItem>
                    </ShipStatWrapper>
                    <ShipStatWrapper>
                        <ShipStatName>Mass:</ShipStatName>
                        <ShipStateDataItem>{this.state.ship.hull.mass}/ {this.state.ship.getFreeMass()}</ShipStateDataItem>
                        <span  style={{marginLeft: "10px"}}>free</span>
                    </ShipStatWrapper>
                    <ShipStatWrapper>
                        <ShipStatName>Cargo:</ShipStatName>
                        <ShipStateDataItem>{this.state.ship.getCargoSpace().toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1´")} tons</ShipStateDataItem>
                    </ShipStatWrapper>
                </ShipStatsContainer>
                <div style={{gridArea: "weapons", display: "grid", gridTemplateColumns: "140px 1fr", gridTemplateAreas: '"title list"'}}>
                    <ListTitle>Weaponry:</ListTitle>
                    <ListContainer>
                        {this.state.ship.armaments.map((item, index) => {
                            return <div key={index} style={{display: "grid", gridTemplateColumns: "2fr 2fr 1fr 1fr"}}>
                                <span>{item.armament.name}</span>
                                <span>{item.armament.damage}</span>
                                <span>{item.armament.special}</span>
                                <span>x{item.amount}</span>
                            </div>
                        })}
                    </ListContainer>
                </div>
                <div style={{gridArea: "defenses", display: "grid", gridTemplateColumns: "140px 1fr", gridTemplateAreas: '"title list"'}}>
                    <ListTitle>Defenses:</ListTitle>
                    <ListContainer>
                        {this.state.ship.defenses.map((item, index) => {
                            return <div key={index} style={{display: "grid", gridTemplateColumns: "2fr 2fr 1fr 1fr"}}>
                                <span>{item.name}</span>
                                <span>{item.special_effect}</span>
                            </div>
                        })}
                    </ListContainer>
                </div>
                <div style={{gridArea: "fittings", display: "grid", gridTemplateColumns: "140px 1fr", gridTemplateAreas: '"title list"'}}>
                    <ListTitle>Fittings:</ListTitle>
                    <ListContainer>
                        {this.state.ship.fittings.map((item, index) => {
                            return <div key={index} style={{display: "grid", gridTemplateColumns: "2fr 3fr 1fr"}}>
                                <span>{item.fitting.name}</span>
                                <span>{item.fitting.notes}</span>
                                <span>x{item.amount}</span>
                            </div>
                        })}
                    </ListContainer>
                </div>
                <div style={{gridArea: "shipyard", display: "grid", gridTemplateColumns: "1fr 1fr"}}>
                    <div style={{display: "grid", gridTemplateColumns: "1fr 1fr"}}><span style={{fontWeight: "bold", fontStyle: "italic"}}>Shipyard:</span><span>{this.state.ship.shipyard}</span></div>
                    <div style={{display: "grid", gridTemplateColumns: "1fr 1fr"}}><span style={{fontWeight: "bold", fontStyle: "italic"}}>Est. Payroll:</span><span>{0}</span></div>
                    <div style={{display: "grid", gridTemplateColumns: "1fr 1fr"}}><span style={{fontWeight: "bold", fontStyle: "italic"}}>Designer:</span><span>{this.state.ship.designer}</span></div>
                    <div style={{display: "grid", gridTemplateColumns: "1fr 1fr"}}><span style={{fontWeight: "bold", fontStyle: "italic"}}>Ships's Stores:</span><span>{this.state.ship.credit_stores}</span></div>
                    <div style={{display: "grid", gridTemplateColumns: "1fr 1fr"}}><span style={{fontWeight: "bold", fontStyle: "italic"}}>Build Date:</span><span>{new Date(this.state.ship.build_date).toDateString()}</span></div>
                    <div style={{display: "grid", gridTemplateColumns: "1fr 1fr"}}><span style={{fontWeight: "bold", fontStyle: "italic"}}>Maintenance:</span><span>{0}</span></div>
                </div>
                <div style={{gridArea: "description"}}>
                    <span style={{fontWeight: "bold", fontStyle: "italic", marginRight: "10px"}}>Description:</span>
                    <span>Free merchants are built for versatility and cargo capacity on a basic frigate hull. While they lack the armor of their military brethren, they have significant free space for owner modifications or larger cargo holds. This particular configuration represents a merchantman with teeth, equipped to ply the more dangerous trade routes. The weaponry gives it a chance to fight its way through an ambush, while the fuel bunkers allow for a fast turnaround drill out of a system if it turns out that the locals aren’t feeling welcoming.</span>
                </div>
            </ShipSheetWrapper>
        )
    }
}