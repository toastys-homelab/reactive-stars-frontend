FROM node:10-alpine AS builder

RUN apk add yarn
COPY . /src
WORKDIR /src
RUN yarn install
RUN yarn build

FROM abiosoft/caddy:latest
COPY --from=builder /src/build /srv
COPY Caddyfile /etc/Caddyfile
EXPOSE 2015